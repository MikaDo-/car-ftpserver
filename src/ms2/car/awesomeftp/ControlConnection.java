package ms2.car.awesomeftp;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class represents the control connection between the client and the server.
 * A Session object must have at least one. It provides further abstraction to
 * respectively read and write Request and Message objects to the client more easily.
 */
public class ControlConnection {
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;
    private FTPLogger logger;

    public ControlConnection(Socket s) throws IOException {
        this.logger = new FTPLogger(FTPLogger.DebugLevel.verbose);
        this.socket = s;
        logger.println("CONNECTION ESTABLISHED : " + s.getInetAddress());
        try {
            this.in  = new BufferedReader(new InputStreamReader(s.getInputStream()));
            this.out = new PrintWriter(s.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("ERROR - Cannot get connection input/output streams.");
            e.printStackTrace();
        }
    }

    /**
     * Reads a line from the connection.
     * @return Next line
     * @throws IOException
     */
    public String readLine() throws IOException {
        String line = this.in.readLine();
        logger.println("IN  << " + line);
        return line;
    }

    /**
     * Sends a string to the client.
     * @param msg Message to be processed.
     */
    public void send(String msg){
        logger.println("OUT >> " + msg);
        this.out.println(msg);
    }

    /**
     * Reads and parses the next Request received from the client.
     * @return Nex Request from the connection.
     */
    public Request getNextRequest() throws DisconnectedClientException {
        String line = null;
        try {
            line = this.readLine();
        } catch (IOException e) {
            System.out.println("ERROR - Cannot parse last request. Maybe the connection was closed. Yeah. Assuming so.");
            throw new DisconnectedClientException(DisconnectedClientException.Reason.connectionClosed);
        }
        return new Request(line);
    }

    public void sendMessage(Message m){
        if(m == null)
            return;
        int lineNum = 0;
        for(String l : m.getContent()) {
            if(m.getContent().size() == ++lineNum)
                this.send(m.getNumber() + " " + l);
            else
                this.send(m.getNumber() + "-" + l);
        }
    }
    public void close() throws IOException {
        this.socket.close();
    }
    public InetAddress getRemoteAddress(){
        return this.socket.getInetAddress();
    }
}
