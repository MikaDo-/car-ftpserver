package ms2.car.awesomeftp;

import java.io.IOException;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class Server implements Runnable{

    public enum Status{
        Initializing, Ready, Listening, Crashed, InitFailure, Terminated
    }
    public static boolean acceptAnonymous = false;
    public static FTPLogger logger = new FTPLogger(FTPLogger.DebugLevel.debug);

    private int port;
    private ArrayList<Session> clients;
    private ArrayList<Integer> usedDataPorts;
    private ServerSocket ss;
    private LoginService loginService;
    private Status status;

    public Server() {
        this(2121);
    }
    public Server(int port) {
        this.status = Status.Initializing;
        this.port = port;
        this.clients = new ArrayList<Session>();
        this.usedDataPorts = new ArrayList<Integer>();
        try {
            this.loginService = new LoginService("users.json");
        } catch (NoSuchAlgorithmException e) {
            logger.println("Error - Cannot initialize login service. Seems like your system does not support SHA-256");
            this.status = Status.InitFailure;
            return;
        }

        try {
            ss = new ServerSocket(this.port);
        } catch (IOException e) {
            System.out.println("FATAL - Cannot listen on port " + this.port + ". Make sure no other application is using it");
            this.status = Status.InitFailure;
            return;
        }
        this.status = Status.Ready;
        this.logger.println("Server up and running !");
    }

    @Override
    public void run() {
        if(this.status != Status.Ready)
            return;
        this.status = Status.Listening;
        while (this.status == Status.Listening) {
            try {
                Session s = new Session(ss.accept(), this);
                clients.add(s);
                s.start();
            } catch (Exception e) {
                logger.println("Whoops, something wrong happened. Force-terminating all sessions and closing port " + this.port + ".");
                for(Session session : this.clients)
                    session.kill();
                this.status = Status.Crashed;
            }
        }
        logger.println("Stopped listening. Shutting down...");
        this.status = Status.Terminated;
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public InetAddress getAddress() throws UnknownHostException {
        return InetAddress.getLocalHost();
    }
}
