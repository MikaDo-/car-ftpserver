package ms2.car.awesomeftp;

import sun.misc.IOUtils;

import java.io.*;
import java.util.Date;

public class UploadFileTransfer extends FileTransfer {

    /**
     * @param parent
     * @param local
     */
    public UploadFileTransfer(Session parent, FilePath local) {
        super(parent, local);

        File f = new File(this.localPath.getFullPath());
        if (f.exists())
            f.delete();
        try {
            f.createNewFile();
        } catch (IOException e) {
            this.status = Status.NotWritable;
            return;
        }
    }

    public void transfer(){
        FilePath path = new FilePath(localPath);

        // if the file does not exist, we create an empty one.
        try {
            FileSystem.touch(path);
        } catch (IOException e) {
            this.status = Status.NotWritable;
            return;
        }

        try {
            this.parent.getDataConnection().initConnection();
        } catch (IOException e) {
            this.status = Status.ConnectionFailed;
            return;
        }

        FileOutputStream stream = null;
        try {
            stream = FileSystem.getFileOutputStream(path);
        } catch (IOException e) {
            // we just created the file so it cannot be a FileNotFoundException
            this.status = Status.UnknownIOError;
            return;
        }
        int c;
        this.startTime = new Date();
        try {
            while( (c = parent.getDataConnection().read()) != -1 && this.status != Status.Canceled){
                try {
                    stream.write(c);
                }catch(IOException e){
                    this.endTime = new Date();
                    this.status = Status.ConnectionBroken;
                    return;
                }
                this.transferred++;
            }
            stream.flush();
        } catch (IOException e) {
            this.status = Status.NotWritable;
            return;
        }
        this.endTime = new Date();
        this.parent.clearCurrentFileTransfer();
        this.status = Status.Done;
    }

}
