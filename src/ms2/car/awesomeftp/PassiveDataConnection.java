package ms2.car.awesomeftp;

import java.io.*;
import java.net.Inet6Address;
import java.net.ServerSocket;
import java.util.Random;

public class PassiveDataConnection extends DataConnection {

    ServerSocket ss;

    public PassiveDataConnection(Session parent, TransferMode mode) throws IOException {
        super(parent, mode);
        this.port = new Random().nextInt((65535 - 1024) + 1) + 1024;
        int tryCounter = 0;
        while(ss == null) {
            try {
                this.ss = new ServerSocket(this.port);
            }
            catch (IOException e) {
                // we should try another port don't we ?
                if(tryCounter++ >= 10)
                    throw e;
            }
        }
    }

    public void initConnection() throws IOException {
        if(this.socket != null) // on a déjà une connexion
            return;
        this.socket = this.ss.accept();
        System.out.println("PASSIVE CONNEXION ESTABLISHED");
        if(this.socket.getInetAddress() instanceof Inet6Address)
            throw new IPv6Exception("IPv6 connections are not supported for data transfer.");
        this.out = new BufferedOutputStream(this.socket.getOutputStream());
        this.in  = new BufferedInputStream(this.socket.getInputStream());
    }

    @Override
    public void close() {
        try {
            this.out.close();
            this.in.close();
            this.socket.close();
            System.out.println("PASSIVE CONNEXION CLOSED");
        } catch (IOException e) {
            // au pire on s'en fout !
        }
    }

    @Override
    public boolean isPassive() {
        return true;
    }

    @Override
    public int read() throws IOException {
        return this.in.read();
    }

    @Override
    public void write(int c) throws IOException {
        out.write(c);
    }

    public void flush() throws IOException {
        out.flush();
    }

    @Override
    public void write(byte[] c, int count) {

    }

    @Override
    public void writeFile(FilePath path) throws IOException {
    }

    public int getPort(){
        return this.ss.getLocalPort();
    }

    public String readLine(){
        return new String("NYI");
    }
}
