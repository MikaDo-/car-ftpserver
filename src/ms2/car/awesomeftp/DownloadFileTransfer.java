package ms2.car.awesomeftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

public class DownloadFileTransfer extends FileTransfer {

    protected long size;
    /**
     * This is meant to be called by RequestProcessor
     * @param parent Parent Session
     * @param local Fullpath of the local target file.
     */
    public DownloadFileTransfer(Session parent, FilePath local) {
        super(parent, local);

        File f = new File(this.localPath.getFullPath());
        if(f.canRead() == false)
            this.status = Status.NotReadable;

        /*if(f.canWrite() == false)
            this.status = Status.NotWritable;*/
        if(f.isDirectory())
            this.status = Status.IsDirectory;
        this.size = f.length();
    }

    @Override
    public void transfer() {

        File f = new File(FilePath.join(parent.pwd(), localPath).getFullPath());

        FileInputStream stream = null;
        try {
            stream = new FileInputStream(localPath.getFullPath());
        } catch (FileNotFoundException e) {
            this.status = Status.NotWritable;
            return;
        }
        try {
            this.parent.getDataConnection().initConnection();
        } catch (IOException e) {
            this.status = Status.ConnectionFailed;
            return;
        }
        int c;
        this.startTime = new Date();
        try {
            while( (c = stream.read()) != -1 && this.status != Status.Canceled)
                try {
                    parent.getDataConnection().write(c);
                    this.transferred++;
                } catch (IOException e) {
                    this.endTime = new Date();
                    this.status = Status.ConnectionBroken;
                    return;
                }
            parent.getDataConnection().flush();
        } catch (IOException e) {
            this.status = Status.NotWritable;
            return;
        }
        this.endTime = new Date();
        parent.getDataConnection().close();

        if(this.status != Status.Canceled)
            this.status = Status.Done;
        this.parent.clearCurrentFileTransfer();
    }

    /**
     * return a 0-1-ranged progress
     */
    public double getProgress(){
        return (double)this.transferred /(double)this.size;
    }

    public long getSize() {
        return size;
    }

}
