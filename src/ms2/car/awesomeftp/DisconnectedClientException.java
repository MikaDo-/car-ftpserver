package ms2.car.awesomeftp;

import java.io.IOException;
import java.util.Dictionary;

/**
 * This exception is thrown when the connection is closed by the client.
 * Typically, this is when Socket.read() returns an incorrect value.
 */
public class DisconnectedClientException extends IOException{

    private Reason reason;

    private DisconnectedClientException(){}

    public DisconnectedClientException(String message){
        super(message);
    }

    public DisconnectedClientException(Reason r){
        super("Client disconnected");
        this.reason = r;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public enum Reason{
        quitReceived, connectionClosed, socketException
    }
}
