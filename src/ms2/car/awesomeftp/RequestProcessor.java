package ms2.car.awesomeftp;

import java.io.*;
import java.net.UnknownHostException;

/**
 * This class does processes Request objects in order to do the actuel work (on the FileSystem, the session/server status...)
 */
public class RequestProcessor {

    private Session parent;
    private Server server;

    private RequestProcessor() {
    }

    public RequestProcessor(Session parent) {
        this.parent = parent;
        this.server = parent.getServer();
    }

    /**
     * Checks whether a correct number of argument is supplied in the request.
     *
     * @param req The request to check.
     * @return The number of arguments supplied is compatible with the command.
     */
    public static boolean checkSyntax(Request req) {
        if (req.getCommand().getArgCount() >= 0)
            return req.getArguments().size() == req.getCommand().getArgCount();
        else
            return req.getArguments().size() <= -1 * (req.getCommand().getArgCount());
    }

    public Message process(Request req) {
        assert (req.getArguments() != null);

        if (this.checkSyntax(req) == false)
            return new Message(501, "Syntax error in parameters or arguments.");

        if (req.getCommand().requiresAuth() && parent.getLoginToken().isValid() == false)
            return new Message(530, "Not logged in.");

        if (parent.getLoginToken().isValid())
            try {
                server.getLoginService().renewToken(parent.getLoginToken());
            } catch (AuthenticationException e) {
                // Impossible car on vient de tester que le token est encore valide.
            }

        Message m;
        switch (req.getCommand()) {
            case USER:
                if (parent.getLoginToken().isValid())
                    m = new Message(530, "You're already logged in.");
                else
                    m = this.processUser(req);
                break;
            case PASS:
                if (parent.getLoginToken().isValid())
                    m = new Message(530, "You're already logged in.");
                else
                    m = this.processPass(req);
                break;
            case CWD:
                m = this.processCwd(req);
                break;
            case CDUP:
                Request cdup = new Request("cwd ..");
                m = this.processCwd(cdup);
                break;
            case PWD:
                m = this.processPwd();
                break;
            case PASV:
                m = this.processPasv();
                break;
            case NOOP:
                m = new Message(200, "Zzz...");
                break;
            case MKD:
                m = this.processMkd(req);
                break;
            case RETR:
                m = this.processRetr(req);
                break;
            case STOR:
                m = this.processStor(req);
                break;
            case TYPE:
                m = new Message(200, "TYPE is now 8-bit binary");
                break;
            case LIST:
                m = this.processList(req);
                break;
            case SYST:
                m = new Message(215, "UNIX Type: L8");
                break;
            case NYI:
                m = new Message(502, "Command not implemented.");
                break;
            case QUIT:
                m = new Message(221, new String[]{"Goodbye.", "Logout"});
                this.parent.terminate();
                break;
            case WRONG_CMD:
            default:
                m = new Message(500, "Unknown command");
                break;
        }
        return m;
    }

    private Message processUser(Request req) {
        String username = req.getArguments().get(0);
        this.parent.getLoginToken().setUsername(username); // we use the current default loginToken to store the user login request.
        return new Message(331, "User " + username + " OK. Password required");
    }

    private Message processPass(Request req) {
        String password = req.getArguments().get(0);
        LoginToken token = null;
        try {
            token = server.getLoginService().requestToken(parent.getLoginToken().getUsername(), password);
        } catch (AuthenticationException e) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e1) {
                // This timer does not handle any long-lasting processing
            }
            return new Message(430, "Invalid username or password");
        }
        parent.setLoginToken(token);
        try {
            parent.cd(new FilePath(token.getChroot()).append(token.getHomedir()));
        } catch (FileNotFoundException e) {
            Message m = new Message(221, new String[]{"Can't change directory to " + new FilePath(token.getHomedir()).chroot(token).getFullPath(), "Logout"});
            this.parent.terminate();
            return m;
        }
        return new Message(230, "Ok. Current directory is " + FilePath.chroot(parent.pwd(), parent.getLoginToken().getChroot()).getFullPath());
    }

    private Message processCwd(Request req) {
        FilePath path = new FilePath(parent.pwd()).append(req.getArguments().get(0));
        try {
            parent.cd(path);
        } catch (FileNotFoundException e) {
            return new Message(550, "Can't change directory to " + path.chroot(parent.getLoginToken()).getFullPath());
        }
        return new Message(230, "Ok. Current directory is " + path.chroot(parent.getLoginToken()).getFullPath());
    }

    private Message processPwd() {
        return new Message(257, "\"" + FilePath.chroot(new FilePath(parent.pwd().getFullPath()), parent.getLoginToken().getChroot()).getFullPath() + "\" is your current location");
    }

    private Message processPasv() {
        DataConnection dataConnection = this.parent.getDataConnection();
        if (dataConnection == null || dataConnection instanceof PassiveDataConnection) {
            try {
                this.parent.createPassiveDataConnection();
            } catch (IOException e) {
                return new Message(425, "Cannot open data connection");
            }
            dataConnection = this.parent.getDataConnection();
        }
        int port = dataConnection.getPort();
        int p1 = port / 256;
        int p2 = port % 256;
        //byte[] addr = new byte[0];
        String[] addr = new String[4];
        try {
            String strTmp = server.getAddress().getHostAddress();
            addr = strTmp.split("\\.");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        //return new Message(227, "Entering Passive Mode (" + addr[0] + "," + addr[1] + "," + addr[2] + "," + addr[3] + "," + p1 + "," + p2 + ")");
        return new Message(227, "Entering Passive Mode (172,18,12,76," + p1 + "," + p2 + ")");
    }

    private Message processRetr(Request req) {
        if (parent.getCurrentFileTransfer() != null)
            return new Message(550, "A data transfer is pending.");
        String path = req.getArguments().get(0);
        if (this.parent.getLoginToken().canRead() == false)
            return new Message(550, "Can't open " + path + ". Permission denied");
        if (parent.getDataConnection() == null)
            return new Message(550, "No data connection.");
        FilePath local = FilePath.join(this.parent.pwd(), new FilePath(path));
        System.out.println("Fullpath : " + local.getFullPath());
        FileTransfer ft = new DownloadFileTransfer(this.parent, local);
        parent.setCurrentFileTransfer(ft);
        ft.start();
        return null;
    }

    private Message processStor(Request req) {
        if (parent.getCurrentFileTransfer() != null)
            return new Message(550, "A data transfer is pending.");
        String path = req.getArguments().get(0);
        if (this.parent.getLoginToken().canWrite() == false)
            return new Message(550, "Can't write to " + path + ". Permission denied");
        if (parent.getDataConnection() == null)
            return new Message(550, "No data connection.");
        FilePath local = FilePath.join(this.parent.pwd(), new FilePath(path));
        System.out.println("Fullpath : " + local.getFullPath());
        FileTransfer ft = new UploadFileTransfer(this.parent, local);
        parent.setCurrentFileTransfer(ft);
        ft.start();
        return null;
    }

    private Message processList(Request req) {
        String path;
        FilePath local = null;
        if (req.getArguments().size() == 0)
            local = this.parent.pwd();
        else {
            path = req.getArguments().get(0);
            local = FilePath.join(this.parent.pwd(), new FilePath(path)).getParentDir();
        }
        if (parent.getDataConnection() == null)
            return new Message(550, "No data connection.");
        try {
            parent.getDataConnection().initConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] entries = FileSystem.getDirContentNames(local);
        BufferedOutputStream out = null;
        /*
        PrintWritter out = null;
        try {
            out = new PrintWriter(parent.getDataConnection().getSocket().getOutputStream());
        } catch (IOException e) {
            return new Message(550, "Connection broken.");
        }*/
        out = parent.getDataConnection().out;
        try {
            for (String entry : entries) {
                out.write(entry.getBytes());
                out.write("\n".getBytes());
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.parent.getDataConnection().close();
        return new Message(200, "Entries listed.");
    }

    private Message processMkd(Request req) {
        String dirName = req.getArguments().get(0);
        if (this.parent.getLoginToken().canRead() == false
            || this.parent.getLoginToken().canWrite() == false)
            return new Message(550, "Can't touch " + dirName + ". Permission denied");
        FilePath dirPath = FilePath.join(this.parent.pwd(), new FilePath(dirName));
        System.out.println("Fullpath : " + dirPath.getFullPath());
        try {
            FileSystem.createDirectory(dirPath);
        } catch (IOException e) {
            e.printStackTrace();
            return new Message(550, "Can't touch " + dirName + ". Permission denied");
        }
        return new Message(200, "Directory created.");
    }
}
