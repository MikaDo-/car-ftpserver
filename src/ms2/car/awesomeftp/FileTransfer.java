package ms2.car.awesomeftp;


import java.util.Date;

/**
 * Abstract class. refer to child classes UploadFileTransfer and DownloadFileTransfer.
 */
public abstract class FileTransfer extends Thread{

    public enum Status{
        Ready, Downloading, Uploading, Done, NoConnection, NotWritable, NotReadable, Canceled, ConnectionBroken, ConnectionFailed, UnknownIOError, IsDirectory
    }

    protected FilePath localPath;
    protected int transferred;
    protected Session parent;
    protected Status status;
    protected Date startTime;
    protected Date endTime;

    /**
     *
     */
    public FileTransfer(Session parent, FilePath local) {
        this.parent = parent;
        this.localPath = new FilePath(local);
        this.status = Status.Ready;
        this.transferred = 0;
        this.startTime = this.endTime = null;
    }

    @Override
    public void run() {
        if(this.status != Status.Ready) {
            this.parent.transferEndNotification(this);
            return;
        }
        if(parent.getDataConnection() == null){
            this.status = Status.NoConnection;
            return;
        }
        this.transfer();
        this.parent.transferEndNotification(this);
    }

    public abstract void transfer();
    /**
     * In milliseconds.
     * @return
     */
    public long getDuration(){
        if(this.endTime != null) // is transfer is over
            return endTime.getTime()- startTime.getTime();
        else
            return new Date().getTime()- startTime.getTime();
    }

    public double getAverageSpeed(){
        return this.transferred /(double)( (this.getDuration()/1000.) );
    }

    public void cancel(){
        this.status = Status.Canceled;
        this.interrupt();
    }

    public Date getStartTime() {
        return startTime;
    }

    public FilePath getLocalPath() {
        return localPath;
    }

    public void setLocalPath(FilePath localPath) {
        this.localPath = localPath;
    }

    public int getTransferred() {
        return this.transferred;
    }

    public Session getParent() {
        return parent;
    }

    public Status getStatus() {
        return this.status;
    }
}
