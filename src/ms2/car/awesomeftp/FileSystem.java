package ms2.car.awesomeftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Map;

import org.apache.commons.io.FileUtils;

/**
 *
 */
public class FileSystem {


    private FilePath filePath;

    /**
     *
     * @param filePath
     */
    public FileSystem(FilePath filePath){

        this.filePath = filePath;

    }

    /**
     *  Return the names of the files inside the taget directory.
     * @param filePath Target directory.
     * @return String[] Basenames
     */
    public static String[] getDirContentNames(FilePath filePath){
        String[] filenames;
        File directory = new File(filePath.getFullPath());
        filenames=directory.list();
        String[] lines = new String[filenames.length];

        for(int i = 0;i<lines.length;i++){
            FilePath fp = new FilePath(filePath, filenames[i]);
            StringBuilder sb = new StringBuilder();
            String rights = null;

            Map attributes = null;
            try {
                attributes = Files.readAttributes(Paths.get(fp.getFullPath()), "*");
            } catch (IOException e) {
                e.printStackTrace();
            }

            char type = '?';
            File f = new File(fp.getFullPath());
            if (f.isDirectory())
                type = 'd';
            else
                type = '-';
            String chmod = null;
            try {
                chmod = PosixFilePermissions.toString(Files.getPosixFilePermissions(f.toPath()));
            } catch (IOException e) {
                System.out.println("Cannot read file permission for file " + fp.getFullPath());
                chmod = "000";
            }
            lines[i] = type + chmod + " 4 alexis alexis " + f.length() + " févr. 15 22:22 " + filenames[i];
        }

        return lines;
    }

    public static void touch(FilePath filePath) throws IOException {
        File file = new File(filePath.getFullPath());
        FileUtils.touch(file);
    }

    public static FileOutputStream getFileOutputStream(FilePath filePath) throws IOException {
        return new FileOutputStream(filePath.getFullPath());
    }

    public static FileInputStream getFileInputStream(FilePath filePath) throws IOException {
        return new FileInputStream(filePath.getFullPath());
    }

    public static void deleteFile(FilePath path) throws FileSystemException {
        File f = new File(path.getFullPath());
        if(f.isFile())
            throw new FileSystemException("Target filename is a directory.");
        f.delete();
    }

    public static void deleteDirectory(FilePath path) throws FileSystemException {
        File f = new File(path.getFullPath());
        assert(f.isDirectory());
        if(FileUtils.sizeOfDirectory(f) != 0)
            throw new FileSystemException("Directory not empty.");
        f.delete();
    }

    public static void createDirectory(FilePath fp) throws IOException {
        File parent = new File(fp.getDirname());
        if(parent.exists() == false)
            throw new IOException("Parent directory doesn't exist");
        if(parent.isFile())
            throw new IOException("Cannot crate a directory within a file");
        File f = new File(fp.getFullPath());
        if(f.exists())
            throw new IOException("Target path already exists");
        else
            f.mkdir();
    }

}