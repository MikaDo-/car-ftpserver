package ms2.car.awesomeftp;

import java.io.File;
import java.lang.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class FilePath {
    private String fullPath;

    public FilePath() {
        this("/");
    }

    public FilePath(String fullPath) {
        this.fullPath = fullPath;
    }

    public FilePath(FilePath original) {
        this.fullPath = original.getFullPath();
    }

    public FilePath(FilePath base, String append){
        this.fullPath = base.getFullPath();
        this.append(append);
    }

    public String getBasename(){
        if(fullPath.equals("/"))
            return "/";
        String[] pathElements = this.fullPath.split("/");
        return pathElements[pathElements.length - 1];
    }

    public String getDirname(){
        if(fullPath.equals("/"))
            return "/";
        String[] pathElements = this.fullPath.split("/");
        int sz = 0, i = 0;
        for(;i<pathElements.length-1;i++)
            sz += pathElements[i].length();
        if(i>1)
            i--;
        return fullPath.substring(0, sz+i);
    }

    public FilePath getParentDir(){
        return new FilePath(this.getDirname());
    }

    public FilePath append(String pathElement){
        if(this.fullPath.equals("/") == false)
            this.fullPath = this.fullPath + "/"+pathElement;
        else
            this.fullPath = pathElement;
        this.cleanPath();
        return this;
    }

    public FilePath append(String[] pathElements){
        for(String element : pathElements) {
            this.append(element);
            this.cleanPath();
        }
        return this;
    }

    public FilePath append(FilePath p){
        String add = p.fullPath;
        if(this.fullPath.equals("/")){
            this.fullPath = add;
            return p;
        }
        if(add.charAt(0) == '/')
            this.fullPath = this.fullPath + add;
        else
            this.fullPath = this.fullPath + "/" + add;
        return this;
    }

    public static FilePath join(FilePath before, FilePath after){
        FilePath tmp = new FilePath();
        tmp.append(before);
        tmp.append(after);
        return tmp;
    }

    /**
     * Used to make sure the user stays within its access restrictions.
     * After that, the filepath will be chrooted.
     * @param user the user we take the chroot from
     * @return this
     */
    public FilePath chroot(User user){
        this.fullPath = FilePath.chroot(this, user.getChroot()).getFullPath();
        return this;
    }

    /**
     * Used to make sure the user stays within its access restrictions.
     * After that, the filepath will be chrooted.
     * @param session The chroot dir will be found within the current session user.
     * @return this
     */
    public FilePath chroot(Session session){
        return this.chroot(session.getLoginToken());
    }

    /**
     * Removes the chroot par from the fullpath. If fullpath if outside chroot, fullpath is set to /
     * @param fp Filepath to secure within the chroot.
     * @param newRoot The path considered as root.
     * @return A new FilePath chrooted object
     */
    public static FilePath chroot(FilePath fp, FilePath newRoot){
        FilePath res = new FilePath();
        ArrayList<String> fpElements = new ArrayList<String>(Arrays.asList(fp.getFullPath().split("/")));
        ArrayList<String> chrootElements = new ArrayList<String>(Arrays.asList(newRoot.getFullPath().split("/")));

        if(chrootElements.size() > fpElements.size())
            return new FilePath();

        Iterator<String> itc = chrootElements.iterator();
        Iterator<String> itp = fpElements.iterator();
        while(itc.hasNext()){
            String ch = itc.next();
            String p = itp.next();
            if(ch.equals(p) == false)
                return new FilePath();
            itp.remove();
        }

        for(String fpElement : fpElements)
            res.append(fpElement);

        return res;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
        this.cleanPath();
    }

    /**
     * Removes all unnecessary .. or . in the fullpath
     */
    public void cleanPath(){
        ArrayList<String> pathElements = new ArrayList<String>(Arrays.asList(this.fullPath.split("/")));
        for (int i = 0;i<pathElements.size();i++){
            if(pathElements.get(i).equals("..")){
                pathElements.remove(i--);
                if(i >= 0)
                    pathElements.remove(i--);
            }else if(pathElements.get(i).equals("."))
                pathElements.remove(i--);
        }
        if(pathElements.size() == 0)
            this.fullPath = "/";
        else {
            Iterator<String> it = pathElements.iterator();
            StringBuilder sb = new StringBuilder();
            it = pathElements.iterator();
            while (it.hasNext()) {
                String current = it.next();
                if (current.isEmpty())
                    continue;
                sb.append("/");
                sb.append(current);
            }
            this.fullPath = sb.toString();
        }
    }

    public boolean isRelative(){
        return !this.isAbsolute();
    }

    public boolean isAbsolute(){
        return this.fullPath.charAt(0) == '/';
    }
}
