package ms2.car.awesomeftp;

import java.util.Date;

/**
 * Provides an ephemeral authentication.
 * These tokens are delivered by the LoginService.
 */
public class LoginToken extends User {

    private Date expiresOn;
    private boolean isAnonnymous;

    public LoginToken(){
        super();
        this.expiresOn = new Date();
        this.expiresOn.setTime(0);
    }

    /**
     * Creates a new LoginToken for User that expires on expiresOn
     * @param user
     * @param expiresOn Date on which the token expires
     */
    public LoginToken(User user, Date expiresOn){
        super(user);
        this.isAnonnymous = user.getUsername().toLowerCase().equals("anonymous");
        this.expiresOn = expiresOn;
    }

    /**
     * Is the token still valid ?
     * @return boolean
     */
    public boolean isValid(){
        return expiresOn.after(new Date());
    }

    public Date getExpirationDate() {
        return expiresOn;
    }

    public void setExpirationDate(Date expiresOn) {
        this.expiresOn = expiresOn;
    }
}
