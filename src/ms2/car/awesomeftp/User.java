package ms2.car.awesomeftp;

public class User {

    private String username;
    private String password;
    private String homeDir;
    private FilePath chroot;
    private boolean canRead;
    private boolean canWrite;

    public User(){
        this.username = new String();
        this.password = new String();
        this.homeDir  = new String();
        this.chroot   = new FilePath();
    }

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, String homeDir){
        this.username = username;
        this.password = password;
        this.homeDir  = homeDir;
        this.chroot   = new FilePath();
    }

    public User(String username, String password, String homeDir, String chroot){
        this.username = username;
        this.password = password;
        this.homeDir  = homeDir;
        this.chroot   = new FilePath(chroot);
    }
    /**
     * Constructeur par copie (utile pour les jetons de connexion...)
     * @param user Utilisateur à reproduire.
     */
    public User(User user){
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.homeDir  = user.getHomedir();
        this.chroot   = user.getChroot();
        this.canRead  = user.canRead();
        this.canWrite = user.canWrite();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPermissions(boolean canRead, boolean canWrite){
        this.canRead = canRead;
        this.canWrite = canWrite;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHomedir() {
        return this.homeDir;
    }

    public void setHomedir(String homedir) {
        this.homeDir = homedir;
    }

    public FilePath getChroot() {
        return new FilePath(this.chroot);
    }

    public boolean canWrite() {
        return canWrite;
    }

    public boolean canRead() {
        return canRead;
    }
}
