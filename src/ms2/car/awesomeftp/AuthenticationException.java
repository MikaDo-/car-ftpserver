package ms2.car.awesomeftp;

/**
 * This exception is thrown in case an error happens during the auth process
 */
public class AuthenticationException extends Throwable {

    public AuthenticationException(){
        super();
    }

    public AuthenticationException(String message){
        super(message);
    }
}
