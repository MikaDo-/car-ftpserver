package ms2.car.awesomeftp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.Socket;

public class Session extends Thread{
    private Server parent;
    private ControlConnection controlConnection;
    private RequestProcessor requestProcessor;
    private FTPLogger logger;
    private boolean isActive;
    private LoginToken loginToken;
    private FilePath currentDir;
    private DataConnection dataConnection;
    private FileTransfer currentFileTransfer;

    /**
     * @param s Connection socket to the client
     * @param parent Parent server to which the session is registered
     * @throws IOException
     */
    public Session(Socket s, Server parent) throws IOException {
        this.controlConnection = new ControlConnection(s);
        this.dataConnection = null;
        this.parent = parent;
        this.requestProcessor = new RequestProcessor(this);
        this.logger = new FTPLogger(/*this, */FTPLogger.DebugLevel.verbose);
        this.isActive = true;
        this.loginToken = new LoginToken();
        this.currentDir = new FilePath(); // default : /
        this.currentFileTransfer = null;
    }

    public LoginToken getLoginToken() {
        return loginToken;
    }

    @Override
    public void run() {
        String str = null;
        int i = 0;
        try {

            this.controlConnection.sendMessage( new Message(220, new String[]{  "========= Welcome " + this.controlConnection.getRemoteAddress() + " =========",
                                                                                "This server is running AwesomeFTP indev.",
                                                                                "Anonymous connections are supported but in read-only mode."}));
            while (this.isActive){
                Request req = controlConnection.getNextRequest();
                this.controlConnection.sendMessage(this.requestProcessor.process(req));
            }
            this.controlConnection.close();
        } catch (IOException e) {
            this.logger.println("ERROR - Connection problem. Terminating session for client " + this.loginToken.getUsername() + ":" + this.controlConnection.getRemoteAddress());
            this.terminate();
        }
    }

    public void transferEndNotification(FileTransfer ft){
        assert(this.currentFileTransfer == ft);
        assert(this.currentFileTransfer instanceof DownloadFileTransfer);
        Message m = null;
        switch(ft.getStatus()){
            case Done:
                String[] lines = new String[2];
                lines[0] = "File successfully transferred";
                lines[1] = (ft.getDuration()/(double)1000) + " seconds (measured here), " + Math.round(ft.getAverageSpeed()) + " bytes per second";
                m = new Message(226, lines);
                break;
            case NoConnection:
                m = new Message(425, "No data connection");
                break;
            case NotWritable:
                m = new Message(550, "Can't open destination file : Not writable.");
                break;
            case NotReadable:
                m = new Message(550, "Can't open destination file : Not readable.");
                break;
            case Canceled:
                m = new Message(226, "Transfer aborted.");
                break;
            case ConnectionBroken:
                m = new Message(500, "Data connection was broken.");
                break;
            case ConnectionFailed:
                m = new Message(500, "Can't establish data connection.");
                break;
            case IsDirectory:
                m = new Message(500, "Cannot download a directory.");
                break;
            case Ready: // We should never have this one righ ? :)
            default:
                m = new Message(500, "Something wring happened...");
        }
        this.controlConnection.sendMessage(m);
        this.dataConnection.close();
        this.dataConnection = null;
    }

    public void cd(FilePath path) throws FileNotFoundException {
        FilePath newDir = new FilePath(path);
        if(newDir.isRelative())
            newDir = FilePath.join(currentDir, newDir);
        newDir.chroot(this.getLoginToken());
        newDir = FilePath.join(this.loginToken.getChroot(), newDir);

        File dir = new File(newDir.getFullPath());
        if(dir.exists() == false)
            throw new FileNotFoundException();
        if(dir.isDirectory() == false)
            throw new FileNotFoundException("Target path is not a directory.");
        this.currentDir = newDir;
    }

    public FilePath pwd(){
        return this.currentDir;
    }

    public ControlConnection getControlConnection() {
        return controlConnection;
    }

    public Server getServer(){
        return parent;
    }

    public boolean isActive(){
        return this.isActive;
    }

    public void terminate(){
        this.isActive = false;
        // TODO : terminer les éventuels transferts de fichiers en cours
        if(this.currentFileTransfer != null)
            this.currentFileTransfer.cancel();
    }

    public void kill(){
        this.terminate();
        this.interrupt();
    }

    public void setLoginToken(LoginToken loginToken) {
        this.loginToken = loginToken;
    }

    public void createPassiveDataConnection() throws IOException {
        assert(this.isActive == false);
        try {
            this.dataConnection = new PassiveDataConnection(this, DataConnection.TransferMode.Binary);
        }catch(IPv6Exception e){
            this.controlConnection.sendMessage(new Message(550, "IPv6 is not supported for data connections."));
            throw e; // we forward the exception
        }
    }

    public void setActive(){
        if(this.isActive == false && this.dataConnection != null){
        }
        this.isActive = true;
    }

    public DataConnection getDataConnection() {
        return dataConnection;
    }

    public void setDataConnection(DataConnection dataConnection) {
        this.dataConnection = dataConnection;
    }

    public FileTransfer getCurrentFileTransfer() {
        return currentFileTransfer;
    }

    public void setCurrentFileTransfer(FileTransfer currentFileTransfer) {
        this.currentFileTransfer = currentFileTransfer;
    }

    public void clearCurrentFileTransfer() {
        this.currentFileTransfer = null;
    }
}
