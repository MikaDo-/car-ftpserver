package ms2.car.awesomeftp;

import java.util.*;

public class Request {

    public enum Command{
        WRONG_CMD(false), NYI(false), ABOR(false), ACCT, ADAT, ALLO, APPE, AUTH, CCC,
        CDUP, CONF, CWD(true, 1), DELE(true, 1), ENC, EPRT, EPSV, FEAT, HELP, LANG,
        LIST(false, -1), LPRT, LPSV, MDTM(true, 1), MIC, MKD(true, 1), MLSD, MLST, MODE, NLST,
        NOOP(false), OPTS, PASS(false, 1), PASV, PBSZ, PORT(true, 1), PROT, PWD, QUIT(false), REIN,
        REST, RETR(true, 1), RMD(true, 1), RNFR(true, 1), RNTO(true, 1), SITE(true, 1), SIZE(true, 1), SMNT, STAT, STOR(true, 1),
        STOU, STRU, SYST(false), TYPE(true, 1), USER(false, 1), XCUP, XMKD, XPWD, XRCP, XRMD,
        XRSQ, XSEM, XSEN;

        private boolean requiresAuth = true;
        /**
         * A negative value means that a varying number of arguments is allowed between 0 and -argCount (inclusive)
         */
        private int argCount = 0;

        private Command(){}
        private Command(boolean requiresAuth){
            this.requiresAuth = requiresAuth;
        }
        private Command(boolean requiresAuth, int argCount) {
            this.requiresAuth = requiresAuth;
            this.argCount = argCount;
        }
        public boolean requiresAuth() {
            return requiresAuth;
        }

        public int getArgCount() {
            return argCount;
        }
    }

    private Command command;
    private ArrayList<String> arguments;

    /**
     * Reprensents the requests transferred by the client in an easy-to-process form.
     * @param req Raw request string? Meant to be read from a ControlConnection
     */
    public Request(String req){
        if(req == null) {
            this.command = Command.QUIT;
            this.arguments = new ArrayList<String>();
            return;
        }

        this.arguments = new ArrayList<String>();
        String[] elements = req.split(" ");
        String cmd = elements[0].toUpperCase();

        if(commandTypes.containsKey(cmd))
            this.command = commandTypes.get(cmd);
        else{
            this.command = Command.WRONG_CMD;
            return;
        }

        if(elements.length > 1)
            for (int i=1; i<elements.length; i++ )
                this.arguments.add(elements[i]);
    }

    public ArrayList<String> getArguments() {
        return arguments;
    }

    public Command getCommand() {
        return command;
    }

    private static final Map<String, Command> commandTypes;
    static{
        commandTypes = new  HashMap<String, Command>();
        commandTypes.put("ABOR", Command.NYI);
        commandTypes.put("ACCT", Command.NYI);
        commandTypes.put("ADAT", Command.NYI);
        commandTypes.put("ALLO", Command.NYI);
        commandTypes.put("APPE", Command.NYI);
        commandTypes.put("AUTH", Command.NYI);
        commandTypes.put("CCC",  Command.NYI);
        commandTypes.put("CDUP", Command.CDUP);
        commandTypes.put("CONF", Command.NYI);
        commandTypes.put("CWD",  Command.CWD);
        commandTypes.put("DELE", Command.NYI);
        commandTypes.put("ENC",  Command.NYI);
        commandTypes.put("EPRT", Command.NYI);
        commandTypes.put("EPSV", Command.NYI);
        commandTypes.put("FEAT", Command.NYI);
        commandTypes.put("HELP", Command.NYI);
        commandTypes.put("LANG", Command.NYI);
        commandTypes.put("LIST", Command.LIST);
        commandTypes.put("LPRT", Command.NYI);
        commandTypes.put("LPSV", Command.NYI);
        commandTypes.put("MDTM", Command.NYI);
        commandTypes.put("MIC",  Command.NYI);
        commandTypes.put("MKD",  Command.MKD);
        commandTypes.put("MLSD", Command.NYI);
        commandTypes.put("MLST", Command.NYI);
        commandTypes.put("MODE", Command.NYI);
        commandTypes.put("NLST", Command.NYI);
        commandTypes.put("NOOP", Command.NOOP);
        commandTypes.put("OPTS", Command.NYI);
        commandTypes.put("PASS", Command.PASS);
        commandTypes.put("PASV", Command.PASV);
        commandTypes.put("PBSZ", Command.NYI);
        commandTypes.put("PORT", Command.NYI);
        commandTypes.put("PROT", Command.NYI);
        commandTypes.put("PWD",  Command.PWD);
        commandTypes.put("QUIT", Command.QUIT);
        commandTypes.put("REIN", Command.NYI);
        commandTypes.put("REST", Command.NYI);
        commandTypes.put("RETR", Command.RETR);
        commandTypes.put("RMD",  Command.NYI);
        commandTypes.put("RNFR", Command.NYI);
        commandTypes.put("RNTO", Command.NYI);
        commandTypes.put("SITE", Command.NYI);
        commandTypes.put("SIZE", Command.NYI);
        commandTypes.put("SMNT", Command.NYI);
        commandTypes.put("STAT", Command.NYI);
        commandTypes.put("STOR", Command.STOR);
        commandTypes.put("STOU", Command.NYI);
        commandTypes.put("STRU", Command.NYI);
        commandTypes.put("SYST", Command.SYST);
        commandTypes.put("TYPE", Command.TYPE);
        commandTypes.put("USER", Command.USER);
        commandTypes.put("XCUP", Command.NYI);
        commandTypes.put("XMKD", Command.NYI);
        commandTypes.put("XPWD", Command.NYI);
        commandTypes.put("XRCP", Command.NYI);
        commandTypes.put("XRMD", Command.NYI);
        commandTypes.put("XRSQ", Command.NYI);
        commandTypes.put("XSEM", Command.NYI);
        commandTypes.put("XSEN", Command.NYI);

    };

}
