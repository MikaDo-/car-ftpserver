package ms2.car.awesomeftp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * Abstract representation of a data connection. Can be active or passive.
 * See child classes ActiveDataConnection and PassiveDataConnection
 */
public abstract class DataConnection {

    public enum TransferMode{
        Binary, ASCII, EBCDIC
    }

    protected BufferedOutputStream out;
    protected BufferedInputStream in;
    protected TransferMode mode;
    protected int port;
    protected Socket socket;
    protected Session parent;

    private DataConnection(){}

    public DataConnection(Session parent, TransferMode mode){
        this.mode = TransferMode.Binary;
        this.parent = parent;
        this.mode = mode;
        this.socket = null;
    }

    public abstract boolean isPassive();
    public abstract int read() throws IOException;
    public abstract void write(int c) throws IOException;
    public abstract void write(byte c[], int count);
    public abstract void writeFile(FilePath path) throws IOException;
    public abstract int getPort();
    public abstract void initConnection() throws IOException;
    public abstract void close();
    public abstract void flush() throws IOException;

    public Socket getSocket() {
        return socket;
    }
}
