package ms2.car.awesomeftp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class LoginService {

    /**
     * How long a session may last ?
     */
    public static final long tokenExpiration = 60000; // One minute
    /**
     * Are we accepting anonymous connections on our server ?
     */
    public static final boolean allowAnonymous = true;

    private Map<String, User> registeredUsers;
    private MessageDigest digest;

    public LoginService(String loginFile) throws NoSuchAlgorithmException {
        this.loadUsers(loginFile);
        this.digest = MessageDigest.getInstance("SHA-256");
    }

    public LoginToken requestAnonymousToken(){
        Date expiresOn = new Date();
        expiresOn.setTime(expiresOn.getTime()+tokenExpiration);
        return new LoginToken(registeredUsers.get("anonymous"), expiresOn);
    }

    protected boolean compareHashes(byte[] hash, byte[] challenge){
        if(hash.length != challenge.length)
            return false;
        for(int i = 0;i<hash.length;i++)
            if(hash[i] != challenge[i])
                return false;
        return true;
    }

    public LoginToken requestToken(String username, String password) throws AuthenticationException{
        if(username.equals("anonymous") || username.isEmpty())
            return requestAnonymousToken();
        User usr = this.loadUser(username);
        byte[] hash = null;
        byte[] challenge = null;
        try {
            challenge = new BigInteger(usr.getPassword(),16).toByteArray();
            hash = digest.digest(password.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // everyone supports UTF-8 right ?
        }

        if(compareHashes(hash, challenge) == false)
            throw new AuthenticationException("Bad username or password");
        Date expiresOn = new Date();
        expiresOn.setTime(expiresOn.getTime()+tokenExpiration);
        return new LoginToken(usr, expiresOn);
    }

    /**
     * Reads user list and permissions from loginFile.
     * @param loginFile
     */
    protected void loadUsers(String loginFile) {
        this.registeredUsers = new HashMap<String, User>();

        FileReader reader;
        JSONTokener cc;
        JSONObject json = null;
        JSONArray users = null;
        try {
            reader = new FileReader(loginFile);
            cc = new JSONTokener(reader);
            json = new JSONObject(cc);
            users = json.getJSONArray("users");
        } catch (JSONException e) {
            System.out.println("ERROR - Check login file syntax.\n");
        } catch (FileNotFoundException e) {
            System.out.println("ERROR - Could not find login file.\n");
            System.out.println("ERROR - Thus no connections will be allowed.\n");
        }
        for(int i = 0;i< users.length();i++){
            String username;
            String password;
            String homedir;
            String chroot;
            String write;
            String read;
            try {
                JSONObject user = (JSONObject) users.get(i);
                username = user.getString("username");
                password = user.getString("password");
                homedir  = user.getString("homedir");
                chroot   = user.getString("chroot");
                write    = user.getString("write");
                read     = user.getString("read");
            } catch (JSONException e1) {
                System.out.println("ERROR - Could not parse user entry n° " + i + " in login file "+ loginFile +" .\n");
                continue;
            }
            User u = new User(username, password, homedir, chroot);
            u.setPermissions(read.equals("yes"), write.equals("yes"));
            this.registeredUsers.put(username, u);
        }
        String publicDir;
        try {
            publicDir = json.getString("publicDir");
        } catch (JSONException e) {
            System.out.println("ERROR - No public directory defined. Anonymous connections won't be allowed.");
            return;
        }
        User anonymous = new User("anonymous", "anon@localhost", "/", publicDir); // They are legion and have good memory. Lol
        anonymous.setPermissions(true, false);
        this.registeredUsers.put("anonymous", anonymous);

    }

    protected User loadUser(String username) throws UserNotFoundException{
        if(this.registeredUsers.containsKey(username))
            return this.registeredUsers.get(username);
        else
            throw new UserNotFoundException();
    }

    public void renewToken(LoginToken token) throws AuthenticationException {
        if(!token.isValid())
            throw new AuthenticationException("Session expired. Please login again.");
        else
            token.setExpirationDate(new Date(token.getExpirationDate().getTime()+LoginService.tokenExpiration));
    }

}
