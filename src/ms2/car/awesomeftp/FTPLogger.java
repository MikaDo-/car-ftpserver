package ms2.car.awesomeftp;

import java.io.PrintStream;

public class FTPLogger {
    public enum DebugLevel{
        quiet(0), errorsOnly(1), verbose(2), debug(3), fullDebug(4);

        private int level;
        private DebugLevel(int level){ this.level = level; }
        public boolean higherThan(DebugLevel lvl){
            return this.level > lvl.level;
        }
        public boolean lowerThanOrEquals(DebugLevel lvl){
            return this.level <= lvl.level;
        }
        public boolean equals(DebugLevel lvl){
            return this.level == lvl.level;
        }
    };

    /**
     * Defines the global logging level for the whole Application.
     */
    public static final DebugLevel globalDebugLevel = DebugLevel.debug;

    private DebugLevel level;
    private PrintStream out;

    private FTPLogger() {}

    /**
     * @param level Defines the default debug level for upcomming messages using this logger
     * @param out Output stream for logging
     */
    public FTPLogger(DebugLevel level, PrintStream out){
        this.out = out;
        this.level = level;
    }
    public FTPLogger(DebugLevel level){
        this.level = level;
        this.out = System.out;
    }

    public void println(String line){
        if(this.level.lowerThanOrEquals(globalDebugLevel))
            this.out.println(line);
    }
}
