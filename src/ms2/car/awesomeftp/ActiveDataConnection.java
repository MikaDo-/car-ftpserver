package ms2.car.awesomeftp;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;

public class ActiveDataConnection extends DataConnection {

    public ActiveDataConnection(Session parent, int port, TransferMode mode) throws IOException {
        super(parent, mode);
        this.port = port;
	// 5 seconds timeout...
        socket.connect(new InetSocketAddress(this.parent.getControlConnection().getRemoteAddress(), port), 5000); 
    }

    @Override
    public boolean isPassive() {
        return false;
    }

    @Override
    public int read() throws IOException {
        return 0;
    }

    @Override
    public void write(int c) throws IOException {

    }

    @Override
    public void write(byte[] c, int count) {

    }

    @Override
    public void writeFile(FilePath path) {

    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public void initConnection() throws IOException {
        // connect vers le client :)))))))))))àà
    }

    @Override
    public void close() {
        try {
            this.socket.close();
        } catch (IOException e) {
            // au pire on s'en fout !
        }
    }

    @Override
    public void flush() {
    }
}
