package ms2.car.awesomeftp;

public class FileSystemException extends Exception {
    public FileSystemException() {
    }
    public FileSystemException(String message) {
        super(message);
    }
}
