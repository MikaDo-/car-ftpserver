package ms2.car.awesomeftp;

import java.io.IOException;

public class IPv6Exception extends IOException {
    public IPv6Exception() {
    }
    public IPv6Exception(String message) {
        super(message);
    }
}
