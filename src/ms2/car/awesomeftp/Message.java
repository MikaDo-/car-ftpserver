package ms2.car.awesomeftp;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Contains a message that can be transferred to the client as well as the corresponding message number.
 */
public class Message {

    private int number;
    private ArrayList<String> content;

    /**
     * @param number Identifier of the message
     * @param content Understandable message for the client. May be several lines.
     */
    public Message(int number, String[] content){
        this.number=number;
        this.content= new ArrayList<String>(Arrays.asList(content));
    }

    /**
     * @param number Identifier of the message
     * @param content Understandable message for the client. May only be  one line long.
     */
    public Message(int number, String content){
        this.number=number;
        this.content= new ArrayList<String>();
        this.content.add(content);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<String> getContent() {
        return content;
    }

    public void setContent(ArrayList<String> content) {
        this.content = content;
    }

}
