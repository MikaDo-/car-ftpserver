package ms2.car.awesomeftp;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;

public class MessageTest extends TestCase {

    @Test
    public void testSetNumber() throws Exception {

        Message mess = new Message(666, "test");
        mess.setNumber(777);
        assertEquals(777, mess.getNumber());

    }

    @Test
    public void testSetContent() throws Exception {

        Message mess = new Message(666, "test");
        ArrayList<String> test = new ArrayList<String>();
        test.add("test1");
        test.add("test2");
        test.add("test3");
        mess.setContent(test);
        assertEquals(test, mess.getContent());

    }
}