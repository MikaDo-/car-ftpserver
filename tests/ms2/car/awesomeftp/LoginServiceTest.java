package ms2.car.awesomeftp;

import junit.framework.TestCase;

public class LoginServiceTest extends TestCase {

    public void testCompareHashes() throws Exception {

        //byte[] tab = new byte[];
        byte[] tab= "test".getBytes();

        //byte[] tabl = new byte[];
        byte[] tab1 = "test".getBytes();

        LoginService ls = new LoginService("/home/m1/beaujet/car-ftpserver/users.json");

        assertEquals(true,ls.compareHashes(tab,tab1));

    }

    public void testRequestToken() throws Exception {

        LoginService ls = new LoginService("/home/m1/beaujet/car-ftpserver/users.json");

        ls.loadUsers("/home/m1/beaujet/car-ftpserver/users.json");


    }

    public void testLoadUsers() throws Exception {

    }

    public void testLoadUser() throws Exception {

        User user = new User();

        LoginService ls = new LoginService("/home/m1/beaujet/car-ftpserver/users.json");

    }

    public void testRenewToken() throws Exception, AuthenticationException {

        LoginService ls = new LoginService("/home/m1/beaujet/car-ftpserver/users.json");

        LoginToken loginToken = ls.requestAnonymousToken();

        ls.renewToken(loginToken);

        assertEquals(true, loginToken.isValid());

    }
}