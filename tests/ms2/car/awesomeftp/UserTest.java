package ms2.car.awesomeftp;

import junit.framework.TestCase;
import org.junit.Test;

public class UserTest extends TestCase {

    private User user = new User("te","st","/test");

    @Test
    public void testSetUsername() throws Exception {

        System.out.println("Test setUsername");

        this.user.setUsername("test");

        assertEquals("test", this.user.getUsername());

    }

    @Test
    public void testSetPassword() throws Exception {

        System.out.println("Test setPassword");

        this.user.setPassword("test");

        assertEquals("test", this.user.getPassword());

    }

    @Test
    public void testSetHomedir() throws Exception {

        System.out.println("Test setHomedir");

        user.setHomedir("/test/te/test1");

        assertEquals("/test/te/test1", this.user.getHomedir());

    }

   /* public void testGetChroot() throws Exception {

    }*/

    public void testCanWrite() throws Exception {

        System.out.println("Test canWrite");

        assertEquals(false, this.user.canWrite());

    }

    public void testCanRead() throws Exception {

        System.out.println("Test canRead");

        assertEquals(false, this.user.canWrite());

    }

}