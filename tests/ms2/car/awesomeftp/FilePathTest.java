package ms2.car.awesomeftp;

import junit.framework.TestCase;
import org.junit.Test;

public class FilePathTest extends TestCase {

    private FilePath filePath = new FilePath("/home");

    @Test
    public void testGetBasename() throws Exception {

        System.out.println("Test du getBasename");

        assertEquals("home", this.filePath.getBasename());

    }

    @Test
    public void testGetDirname() throws Exception {

        System.out.println("Test du getDirname");

        assertEquals("/", this.filePath.getDirname());

    }

    @Test
    public void testAppend() throws Exception {

        System.out.println("Test du append");

        FilePath p = new FilePath("/test");

        this.filePath.append(p);

        assertEquals("/home/test", this.filePath.getFullPath());

    }

    @Test
    public void testJoin() throws Exception {

        System.out.println("Test du join");

        FilePath after = new FilePath("test/t.txt");

        this.filePath = this.filePath.join(this.filePath,after);

        assertEquals("/home/test/t.txt", this.filePath.getFullPath());

    }

    @Test
    public void testChroot() throws Exception {

        System.out.println("Test du chroot");

        FilePath fp = new FilePath("/home/test/t/");

        User user = new User("user","pass","/home/test");

        fp.chroot(user);

        assertEquals("/home/test/t",fp.getFullPath());

    }

    @Test
    public void testSetFullPath() throws Exception {

        System.out.println("Test du setFullPath");

        this.filePath.setFullPath("/test");

        assertEquals("/test", this.filePath.getFullPath());

    }

}