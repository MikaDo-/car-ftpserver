package ms2.car.awesomeftp;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.Date;

public class LoginTokenTest extends TestCase {

    public void testIsValid() throws Exception {

        System.out.println("Test isValid");

        LoginToken lt = new LoginToken();

        assertEquals(false, lt.isValid());

    }

    @Test
    public void testSetExpirationDate() throws Exception {

        System.out.println("Test setExpirationDate");

        LoginToken lt = new LoginToken();

        lt.setExpirationDate(new Date());

        assertEquals(new Date(), lt.getExpirationDate());

    }
}